package;

import openfl.errors.Error;
import openfl.text.Font;
import openfl.Assets;

/**
 * ...
 * @author Igor Skotnikov
 */
class GameAssets
{
	private static var _instance:GameAssets = new GameAssets();
	public var font(default, null):Font;
	public function new() 
	{
		if (_instance!=null) throw new Error("Singleton and can only be accessed through Singleton.getInstance()");
		else {
			_instance = this;
			font = Assets.getFont("font/Roboto-Medium.ttf");
		}
	}
	
	public static function getInstance():GameAssets {
		return _instance;
	}
}