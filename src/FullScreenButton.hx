package;

import openfl.geom.Rectangle;
import ui.BlackSkinButton;
import openfl.display.Bitmap;
import openfl.display.Sprite;
import openfl.Lib;
import openfl.text.TextFormat;
import govnoknopka.button.Button;
import openfl.display.PixelSnapping;
import openfl.Assets;

/**
 * ...
 * @author Igor Skotnikov
 */
class FullScreenButton extends Button 
{

	public function new() 
	{
		super(new BlackSkinButton(), new Rectangle(0,0,800,480), new TextFormat(GameAssets.getInstance().font.fontName, 12, 0xFFFF00));
		this.setSize(30, 30);
		iconOver	= new Bitmap(Assets.getBitmapData("img/FullScreenIconOverAsset.png"), PixelSnapping.AUTO, true);
		iconDown	= new Bitmap(Assets.getBitmapData("img/FullScreenIconDownAsset.png"), PixelSnapping.AUTO, true);
		iconUp 		= new Bitmap(Assets.getBitmapData("img/FullScreenIconUpAsset.png"),	  PixelSnapping.AUTO, true);
		iconHeight	= 20;
		iconWidth	= 20;
		
		toolTip = createToolTip(new TextFormat(GameAssets.getInstance().font.fontName, 12, 0x222222), "Full Screen");
	}

}
