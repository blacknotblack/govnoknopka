package;

import govnoknopka.layout.HGroup;
import openfl.display.Sprite;
import openfl.events.MouseEvent;
import openfl.display.StageDisplayState;
import js.Browser;

/**
 * ...
 * @author Igor Skotnikov
 */
class Main extends Sprite 
{
	
	var w:Float = 800;
	var h:Float = 480;
	public function new() 
	{
		super();
		
		var b:FullScreenButton = new FullScreenButton();
		b.x = 20;
		b.y = 20;
		var lay:HGroup = new HGroup("id", "right");
		lay.add(b);
		//lay.setSize(600, 90);
		//lay.layout(this);
		
		var b2:FullScreenButton = new FullScreenButton();
		b2.addEventListener(MouseEvent.CLICK, function(e) { trace('' ); untyped __js__ ("fullscreen()"); } );
		var lay1:HGroup = new HGroup("id", "left", 5);
		lay1.add(b2);
		lay1.add(b);
		lay1.setSize(600, 90);
		lay1.layout(this);
		
		Browser.window.fullScreen = true;
		

	}

}
