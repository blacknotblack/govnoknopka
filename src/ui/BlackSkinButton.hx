package ui;

import govnoknopka.skin.SkinButton;

import openfl.display.Bitmap;
import openfl.display.DisplayObject;
import openfl.Assets;

/**
 * ...
 * @author Igor Skotnikov
 */
class BlackSkinButton extends SkinButton
{
	
	public function new() 
	{
		super();
		skinOver = new Bitmap(Assets.getBitmapData("img/BlackButtonOverAsset.png"));
		skinUp = new Bitmap(Assets.getBitmapData("img/BlackButtonDisabledAsset.png"));
		skinDown = new Bitmap(Assets.getBitmapData("img/BlackButtonDownAsset.png"));
	}
	
}