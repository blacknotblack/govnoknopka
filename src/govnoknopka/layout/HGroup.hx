package govnoknopka.layout;

import flash.display.DisplayObject;
import openfl.display.Sprite;

/**
 * ...
 * @author Igor Skotnikov
 */
class HGroup
{
	
	//public var items(default, null):Array<Dynamic>;
	public var itemsLeft(default, null):Array<DisplayObject>;
	public var itemsRight(default, null):Array<DisplayObject>;
	
	
	public var id(default, null):String;
	public var align(default, null):String;
	public var gap(default, null):Int;
	
	private var parent:Sprite;
	private var width:Float;
	private var height:Float;
	public function new(id:String, align:String = "left", gap:Int=0)
	{
		//items = [];
		itemsLeft = [];
		itemsRight = [];
		this.id = id;
		this.align = align;
		this.gap = gap;
	}
	
	public function add(item:Dynamic):Void
	{
		if (Std.is(item, HGroup)) {
			//items.unshift(item);
			var hgroup = cast(item, HGroup);
			for (it in hgroup.itemsLeft) {
				addItem(it, cast(item, HGroup).align);
			}
			for (it in hgroup.itemsRight) {
				addItem(it, cast(item, HGroup).align);
			}
		}
		if (Std.is(item, DisplayObject)) {
			addItem(item, align);
		}
	}
	private function addItem(item:Dynamic, align:String):Void {
		switch(align) {
			case "left":
				itemsLeft.push(item);
			case "right":
				itemsRight.unshift(item);
		}
	}
	
	public function setSize(width, height) {
			this.width = width;
			this.height = height;
			if(parent != null) layout(parent);
	}
	
	public function layout(container:Sprite):Void {
		parent = container;
		for (i in 0...itemsLeft.length) {
			itemsLeft[i].y = parent.y+height/2-itemsLeft[i].height/2;
			itemsLeft[i].x = parent.x;
			if (i != 0) itemsLeft[i].x = itemsLeft[i - 1].x + itemsLeft[i - 1].width;
			itemsLeft[i].x += gap;
			parent.addChild(itemsLeft[i]);
		}
		for (i in 0...itemsRight.length) {
			itemsRight[i].y = parent.y+height/2-itemsRight[i].height/2;
			itemsRight[i].x = width -itemsRight[i].width;
			if (i != 0) itemsRight[i].x = itemsRight[i - 1].x - itemsRight[i].width;
			itemsRight[i].x -= gap;
			parent.addChild(itemsRight[i]);
		}
	}
}