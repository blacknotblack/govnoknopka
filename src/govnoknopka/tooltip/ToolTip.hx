package govnoknopka.tooltip;

import openfl.display.Sprite;
import openfl.geom.Point;
import openfl.geom.Rectangle;
import openfl.text.TextField;
import openfl.text.TextFormat;
import openfl.text.TextFormatAlign;

/**
 * ...
 * @author Igor Skotnikov
 */
class ToolTip extends Sprite
{

	public var text(default, set):String;
	public var tooltip:Sprite;
	
	private var align:String;
	
	private var textfield:TextField;
	private var textFormat:TextFormat;
	
	//GETTER & SETTER
	//-----------textfield--------------
	function set_text(newtext:String) {
		text = newtext;
		invalidateSize();
		return text;
	}
	
	public function new(_textformat:TextFormat, _text:String, _align:String = 'auto')
	{
		super();
		mouseEnabled = false;
		mouseChildren = false;
		alpha = 0;
		
		align = _align;
		
		tooltip = new Sprite();
		addChild(tooltip);
		
		textFormat = _textformat;
		textFormat.align = TextFormatAlign.CENTER;
		textfield = new TextField();
		textfield.defaultTextFormat = _textformat;
		textfield.embedFonts = true;
		textfield.selectable = false;
		textfield.text = text = _text;
		//textfield.x = -10;
		//textfield.y = -22-textfield.height/2;
		addChild(textfield);

	}
	
	public function updatePosition(_x:Float, _y:Float, bounds:Rectangle) {
		x = _x;
		y =	_y;
		var globalMouse:Point = parent.localToGlobal(new Point(_x, _y));
		_x = globalMouse.x;
		_y = globalMouse.y;
		if (align == ToolTipAlign.AUTO) {
			var middleOfBounds:Point = new Point(bounds.x +bounds.width / 2, bounds.y +bounds.height / 2);
			if (_x < middleOfBounds.x && _y < middleOfBounds.y) align = ToolTipAlign.TOP_LEFT;
			if (_x < middleOfBounds.x && _y > middleOfBounds.y) align = ToolTipAlign.BOTTOM_LEFT;
			if (_x > middleOfBounds.x && _y < middleOfBounds.y) align = ToolTipAlign.TOP_RIGHT;
			if (_x > middleOfBounds.x && _y > middleOfBounds.y) align = ToolTipAlign.BOTTOM_RIGHT;
			drawTooltip();
			align = ToolTipAlign.AUTO;
		}
	}
	
	private function drawTooltip():Void {
		tooltip.graphics.clear();
		tooltip.graphics.lineStyle(1.5, 0x9C902D);
		tooltip.graphics.beginFill(0xFFFFCF, 1);
		
		switch(align) {
			case ToolTipAlign.BOTTOM_LEFT:
				textfield.x = -10;
				textfield.y = -22-textfield.height/2;
				tooltip.graphics.lineTo(-5, -7);
				tooltip.graphics.lineTo(-10, -7);
				tooltip.graphics.curveTo(-15, -7, -15, -12);
				tooltip.graphics.lineTo(-15, -12 - textfield.height);
				tooltip.graphics.curveTo(-15, -17 - textfield.height, -10, -17 - textfield.height);
				tooltip.graphics.lineTo(-10 + textfield.width, -17 - textfield.height);
				tooltip.graphics.curveTo(-5 + textfield.width, -17 - textfield.height, -5 + textfield.width, -12 - textfield.height);
				tooltip.graphics.lineTo(-5 + textfield.width, -12);
				tooltip.graphics.curveTo(-5 + textfield.width, -7, -10 + textfield.width, -7); 
				tooltip.graphics.lineTo(5, -7);
				tooltip.graphics.lineTo(0, 0);
				tooltip.graphics.endFill();
			case ToolTipAlign.BOTTOM_RIGHT:
				textfield.x = 10-textfield.width;
				textfield.y = -22-textfield.height/2;
				tooltip.graphics.lineTo(-5, -7);
				tooltip.graphics.lineTo(10 - textfield.width, -7);
				tooltip.graphics.curveTo(5 - textfield.width, -7, 5 - textfield.width, -12);
				tooltip.graphics.lineTo(5 - textfield.width, -12 - textfield.height);
				tooltip.graphics.curveTo(5 - textfield.width, -17 - textfield.height, 10 - textfield.width, -17 - textfield.height);
				tooltip.graphics.lineTo(10, -17 - textfield.height);
				tooltip.graphics.curveTo(15, -17 - textfield.height, 15, -12 - textfield.height);
				tooltip.graphics.lineTo(15, -12);
				tooltip.graphics.curveTo(15, -7, 10, -7);
				tooltip.graphics.lineTo(5, -7);
				tooltip.graphics.lineTo(0, 0);
			case ToolTipAlign.TOP_LEFT:
				textfield.x = -10;
				textfield.y = 20-textfield.height/2;
				tooltip.graphics.lineTo(5, 7);
				tooltip.graphics.lineTo(-10 + textfield.width, 7);
				tooltip.graphics.curveTo(-5 + textfield.width, 7, -5 + textfield.width, 12);
				tooltip.graphics.lineTo(-5 + textfield.width, 12 + textfield.height);
				tooltip.graphics.curveTo(-5 + textfield.width, 17 + textfield.height, -10 + textfield.width, 17 + textfield.height);
				tooltip.graphics.lineTo(-10, 17 + textfield.height);
				tooltip.graphics.curveTo(-15, 17 + textfield.height, -15, 12 + textfield.height);
				tooltip.graphics.lineTo(-15, 12);
				tooltip.graphics.curveTo(-15, 7, -10, 7);
				tooltip.graphics.lineTo(-5, 7);
				tooltip.graphics.lineTo(0, 0);
			case ToolTipAlign.TOP_RIGHT:
				textfield.x = 10-textfield.width;
				textfield.y = 20-textfield.height/2;
				tooltip.graphics.lineTo(5, 7);
				tooltip.graphics.lineTo(10, 7);
				tooltip.graphics.curveTo(15, 7, 15, 12);
				tooltip.graphics.lineTo(15, 12 + textfield.height);
				tooltip.graphics.curveTo(15, 17 + textfield.height, 10, 17 + textfield.height);
				tooltip.graphics.lineTo(10 - textfield.width, 17 + textfield.height);
				tooltip.graphics.curveTo(5 - textfield.width, 17 + textfield.height, 5 - textfield.width, 12 + textfield.height);
				tooltip.graphics.lineTo(5 - textfield.width, 12);
				tooltip.graphics.curveTo(5 - textfield.width, 7, 10 - textfield.width, 7);
				tooltip.graphics.lineTo(-5, 7);
				tooltip.graphics.lineTo(0, 0);
		}
	}
	
	private function invalidateSize():Void {
		if(textfield!=null) {
			if(text!=null) 		textfield.text = text;
			textfield.width  = textfield.textWidth+10;
			textfield.height = textfield.textHeight+2;
			drawTooltip();
		}
	}
}