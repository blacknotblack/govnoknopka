package govnoknopka.tooltip;

/**
 * ...
 * @author Igor Skotnikov
 */
class ToolTipAlign
{

	inline public static var TOP_LEFT:String = 'topLeft';
	inline public static var TOP_RIGHT:String = 'topRight';
	inline public static var BOTTOM_LEFT:String = 'bottomLeft';
	inline public static var BOTTOM_RIGHT:String = 'bottomRight';
	inline public static var AUTO:String = 'auto';
	
}