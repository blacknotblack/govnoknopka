package govnoknopka.button;
import openfl.geom.Rectangle;

/**
 * ...
 * @author Igor Skotnikov
 */
interface IButton
{
	function updateStageBounds(_stageBounds:Rectangle):Void;
}