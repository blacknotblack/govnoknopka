package govnoknopka.button;

import govnoknopka.skin.SkinButton;
import govnoknopka.tooltip.ToolTip;
import openfl.display.DisplayObject;
import openfl.display.Sprite;
import openfl.events.MouseEvent;
import openfl.geom.Rectangle;
import openfl.text.TextField;
import openfl.text.TextFormat;
import openfl.text.TextFormatAlign;
import openfl.events.Event;

import motion.Actuate;

/**
 * ...
 * @author Igor Skotnikov
 */
class Button extends Sprite implements IButton
{

	public var iconOver(default, set)	:DisplayObject;
	public var iconUp(default, set)		:DisplayObject;
	public var iconDown(default, set)	:DisplayObject;
	
	public var iconWidth(default, set):Float;
	public var iconHeight(default, set):Float;
	
	public var text(default, set):String;
	public var textSize(default, set):Int;
	
	public var toolTip:ToolTip;
	
	private var skin:SkinButton;
	
	private var textfield:TextField;
	private var textFormat:TextFormat;
	
	private var stageBounds:Rectangle;
	
	//GETTER & SETTER
	//-----------textfield--------------
	function set_text(newtext:String) {
		text = newtext;
		invalidateSize();
		return text;
	}
	function set_textSize(newtextSize:Int) {
		textSize = newtextSize;
		invalidateSize();
		return textSize;
	}
	//--------------icon----------------
	function set_iconOver(newIcon:DisplayObject) {
		if (iconOver != null && contains(iconOver)) removeChild(iconOver);
		iconOver = newIcon;
		iconOver.visible = false;
		addChild(iconOver);
		return iconOver;
	}
	function set_iconUp(newIcon:DisplayObject) {
		if (iconUp != null && contains(iconUp)) removeChild(iconUp);
		iconUp = newIcon;
		addChild(iconUp);
		return iconUp;
	}
	function set_iconDown(newIcon:DisplayObject) {
		if (iconDown != null && contains(iconDown)) removeChild(iconDown);
		iconDown = newIcon;
		iconDown.visible = false;
		addChild(iconDown);
		return iconDown;
	}
	function set_iconWidth(w:Float) {
		iconDown.width = iconUp.width = iconOver.width = iconWidth = w;
		iconDown.x = iconUp.x = iconOver.x = (this.width - w)/2;
		return iconWidth;
	}
	function set_iconHeight(h:Float) {
		iconDown.height = iconUp.height = iconOver.height = iconHeight = h;
		iconDown.y = iconUp.y = iconOver.y = (this.height - h)/2;
		return iconHeight;
	}
	/**
	 * 
	 * @param	_skin			- скин кнопки
	 * @param	_stageBounds	- область для выравнивания тултипа при Автовыравнивании
	 * @param	_textformat		- формат текста
	 * @param	_text			- текст. Если null, тултип не появляется
	 */
	public function new(_skin:SkinButton, _stageBounds:Rectangle, ?_textformat:TextFormat, ?_text:String)
	{
		super();
		
		stageBounds = _stageBounds;
		mouseChildren = false;
		addEventListener(MouseEvent.MOUSE_OVER, MOUSE_HANDLER, false, 0 , true);
		addEventListener(MouseEvent.MOUSE_OUT, 	MOUSE_HANDLER, false, 0 , true);
		addEventListener(MouseEvent.MOUSE_DOWN, MOUSE_HANDLER, false, 0 , true);
		addEventListener(MouseEvent.MOUSE_UP,	MOUSE_HANDLER, false, 0 , true);
		addEventListener(MouseEvent.MOUSE_MOVE,	MOUSE_HANDLER, false, 0 , true);
		addEventListener(Event.ADDED_TO_STAGE,			ADDED_HANDLER, false, 0 , true);
		addEventListener(Event.REMOVED_FROM_STAGE,		REMOVED_HANDLER, false, 0, true);
		
		skin = _skin;
		addChild(skin.skinDown);
		addChild(skin.skinOver);
		addChild(skin.skinUp);
		skin.skinDown.visible = false;
		skin.skinOver.visible = false;
		if(_textformat != null){
			textFormat = _textformat;
			textFormat.align = TextFormatAlign.CENTER;
			textfield = new TextField();
			textfield.defaultTextFormat = _textformat;
			textfield.embedFonts = true;
			textfield.selectable = false;
			addChild(textfield);
			text = _text;
		}
	}
	public function createToolTip(_textformat:TextFormat, _text:String, ?_align:String):ToolTip {
		return toolTip = new ToolTip(_textformat, _text, _align);
	}
	
	public function setSize(width:Float, height:Float):Void {
		skin.skinOver.width = skin.skinDown.width = skin.skinUp.width = width;
		skin.skinOver.height = skin.skinDown.height = skin.skinUp.height = height;
		if(textfield!=null) {
			textfield.x = (skin.skinDown.width - textfield.width) / 2;
			textfield.y = (skin.skinDown.height - textfield.height) / 2;
		}
	}
	
	public function updateStageBounds(_stageBounds:Rectangle):Void {
		stageBounds = _stageBounds;
	}
	
	private function ADDED_HANDLER(e:Event) {
		if (toolTip != null) parent.addChild(toolTip);
	}
	private function REMOVED_HANDLER(e:Event) {
		if (toolTip != null) parent.removeChild(toolTip);
	}
	
	private function MOUSE_HANDLER(e:MouseEvent):Void {
		switch(e.type) {
			case MouseEvent.MOUSE_OVER | MouseEvent.MOUSE_UP:
				if (iconOver != null) 	iconOver.visible = true; 
				if (iconUp != null) 	iconUp.visible = false; 
				if (iconDown != null) 	iconDown.visible = false;
				skin.skinOver.visible = true; 
				skin.skinUp.visible = false; 
				skin.skinDown.visible = false;
				if (e.type == MouseEvent.MOUSE_OVER && toolTip != null) {
					Actuate.tween(toolTip, 0.1, { alpha:1 } );
					stage.addChild(toolTip);
				}
			case MouseEvent.MOUSE_OUT:
				if (iconOver != null) 	iconOver.visible = false; 
				if (iconUp != null) 	iconUp.visible = true; 
				if (iconDown != null) 	iconDown.visible = false;
				skin.skinOver.visible = false; 
				skin.skinUp.visible = true; 
				skin.skinDown.visible = false;
				if (toolTip!=null) {
					Actuate.tween(toolTip, 0.1, { alpha:0 } );
				}
			case MouseEvent.MOUSE_DOWN:
				if (iconOver != null) 	iconOver.visible = false; 
				if (iconUp != null) 	iconUp.visible = false; 
				if (iconDown != null) 	iconDown.visible = true;
				skin.skinOver.visible = false; 
				skin.skinUp.visible = false; 
				skin.skinDown.visible = true;
			case MouseEvent.MOUSE_MOVE:
				if(toolTip != null){
					toolTip.updatePosition(e.stageX, e.stageY, stageBounds);
				}
		}
	}
	
	private function invalidateSize():Void {
		if(textfield!=null) {
			if(textSize!= 0)	textFormat.size = textSize;
			textfield.defaultTextFormat = textFormat;
			if(text!=null) 		textfield.text = text;
			textfield.width = skin.skinDown.width = skin.skinOver.width = skin.skinUp.width = textfield.textWidth + 10;
			textfield.height = textfield.textHeight+5;
			textfield.x = (skin.skinDown.width - textfield.width) / 2;
			textfield.y = (skin.skinDown.height - textfield.height) / 2;
		}
	}
}